# Prerequisites

- npm 6+
- node.js 10+
- RabbitMQ

### .env (for local environment variables)

Ensure that you have a .env file in the root directory.
Check your own port for RabbitMQ and replace it. on ACMQP_URL

Then add this lines in .env:


ACMQP_URL=amqp://localhost:5672

MONGO_USER=TestingUser

MONGO_PASSWORD=MasKGxRkyPAX9EMqz

MONGO_PATH=cluster0-t8opr.gcp.mongodb.net/exmerdev_activity

PORT=3003


# Getting started

1. `npm install`
2. `npm run dev`


# Edpoints for testing

1. GET `localhost:3003/getUser?id=<id>` (replace "< id >" with the user´s id, you can found it on console log)