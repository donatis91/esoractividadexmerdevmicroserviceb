import { Request, Response, NextFunction } from "express";
import { HTTP400Error } from "../../utils/httpErrors";
import { getUser } from "./usersController";

const checkParams = (req: Request, res: Response, next: NextFunction) => {
  const { id } = req.query;
  if (!id  ) {
    throw new HTTP400Error('Missing parameters: id');
  } else {
    next();
  }
};

export default [
  {
    path: "/getUser",
    method: "get",
    handler: [
      checkParams,
      (req: Request, res: Response, next: NextFunction) => {
        next();
      },
      async (req: Request, res: Response) => {
        let idUser = "";
        if (req.query["id"]) {
          idUser = req.query.id.toString();
          await getUser(idUser).then((d) => {
            res.status(200).send(d);
          });
          
        }
      },
    ],
  },
];
