import { dbClient } from "../../config/mongodb";
import { microServiceBData } from "./interfaces";
import { subscribe } from "../../config/messenger";

const users = dbClient.get("users");

subscribe((data: any) => {
  setTimeout(() => {
    setUser(data).then((d) => {
      return "OK";
    });
  }, 3000); 
});

const setUser = async (data: microServiceBData) => {
  console.log("Writting on DB: ", data);
  return await users.update(
    { _id: data["id"] },
    {
      $set: {
        name: data["name"],
        lastname: data["lastname"],
        phone: data["phone"],
      },
    }
  );
};

export const getUserDB = async (idUser: String) => {
  console.log("Getting user with id: ", idUser);
  return await users.find({ _id: idUser });
};
