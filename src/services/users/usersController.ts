import { getUserDB } from "./usersModel";

export const getUser = async (idUser: String) => {
  return await getUserDB(idUser).then((d: any) => {
    return d;
  });
};
