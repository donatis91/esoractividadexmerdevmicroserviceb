import {
  handleCors,
  handleBodyRequestParsing,
  handleCompression,
  handleCookie,
} from './common';



export default [
  handleCors,
  handleBodyRequestParsing,
  handleCompression,
  handleCookie,
];
