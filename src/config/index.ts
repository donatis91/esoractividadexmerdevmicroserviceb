import { init as initDb } from './mongodb';
import { init as initMessaging } from './messenger';

const initDependencies = async () => {
  await initDb();
  await initMessaging();
};

export { initDependencies };

