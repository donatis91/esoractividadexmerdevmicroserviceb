import dotenv from 'dotenv';

dotenv.config();

const monk = require('monk');
const { MONGO_USER } = process.env;
const { MONGO_PASSWORD } = process.env;
const { MONGO_PATH } = process.env;
const url = 'mongodb+srv://'+MONGO_USER+':'+MONGO_PASSWORD+'@'+MONGO_PATH;

const dbClient =  monk(url);

const init = async () => {
  await dbClient.then(() => {
    console.log("Conected to MongoDB");
  })
  
};

export { init, dbClient };
